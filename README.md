# protos

## How to compile protos into python classes:
1. Install protoc compiler on your system
2. pip install protobuf
3. cd your_project/protos
4. protoc -I=./proto_files --python_out=. ./proto_files/*.proto
